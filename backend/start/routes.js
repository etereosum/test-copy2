'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.group(() => {

    Route.on('/').render('welcome')

    // Auth

    Route.post('/register','AuthController.register')
    Route.post('/login','AuthController.login')

    // User

    Route.get('/user', 'UserController.show').middleware('auth')
    Route.put('/user/:id', 'UserController.update').middleware('auth')
    Route.delete('/user/:id', 'UserController.delete').middleware('auth')


}).prefix('api')

