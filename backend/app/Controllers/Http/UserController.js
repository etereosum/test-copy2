'use strict'
const User = use('App/Models/User');

class UserController {

    async show ({ auth, response }) {
        let user = await auth.getUser();
        return response.json({
            success: true,
            dat: user
        });
    }

    async update ({ request, auth, params, response }) {

        await User
            .query()
            .where( 'id', params.id )
            .update( request.all() );

        let user = await User.find(params.id);

        return response.json(user);
    }

    async delete ({ request, auth, params, response }) {
        let user = await User.find( params.id )
        await user.delete();
        return response.json({ message: 'User has been deleted' })
    }
    


}

module.exports = UserController
